<?php
require_once('../layout/UserLayout.php');
require_once('../ruler/UserRuler.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );
$headers = getallheaders();
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
$request = array();
if($method == 'POST'){
    $request['request_type'] = 'POST';
    $request['content'] = json_decode(file_get_contents("php://input"));
    //print_r($request['content']);
    $request['headers'] = $headers;
    $request['name'] = $uri[5];
}
if($method == 'GET'){
    $request['request_type'] = 'GET';
    $request['headers'] = $headers;
    $request['name'] = $uri[5];
    $request['content'] = $_GET;
}

$service = new UserRuler();

if($request['name'] == 'getUserByEmail'){
   $email = trim(strip_tags($request['content']['email']));
    $user = $service->getUserByEmail($email);
    if($user == null || $user == false || $user->getEmail() == false){
        //print '{"error": "Nincs ilyen user"}';
        http_response_code(204);
        exit();
    }
    $user = new UserLayout($user);
    $user = json_encode($user);
    print $user;
    exit();
}

if ($request['name'] == 'login') {
    $email = trim(strip_tags($request['content']->email));
    $passwd = sha1(trim(strip_tags($request['content']->passwd)));
    $user = $service->login($email, $passwd);
    if($user == null || $user == false || $user->getEmail() == false){
        http_response_code(401);
        exit();
    }
    $user = new UserLayout($user);
    $user = json_encode($user);
    $_SESSION['admin'] = 1;
    print $user;
    exit();
}

if($request['name'] == 'registration'){
    $email = trim(strip_tags($request['content']->email));
    $name = trim(strip_tags($request['content']->name));
    $passwd = sha1(trim(strip_tags($request['content']->passwd)));
    $success = $service->registration($email, $name, $passwd);
    if($success){
        print '{"result": "Sikeres regisztráció, nézd meg az email fiókod!"}';
    }
    else{
        print '{"result": "Sikertelen regisztráció!"}';
    }
    exit();
}

if ($request['name'] == 'getSession') {
    Auth::authorize();
    print_r($service->getSession());
    exit();
}

if ($request['name'] == 'getCurrentUser') {
    Auth::authorize();
    if($user = $service->getCurrentUser()){
        $user = new UserLayout($user);
        $user = json_encode($user);
        print $user;
    }
    else{
        http_response_code(401);
    }
    exit();
}

if($request['name'] == 'logout'){
    session_destroy();
    exit();
}

if ($request['name'] == 'changePassword') {
    Auth::authorize();
    $oldPasswd = trim(strip_tags($request['content']->oldPasswd));
    $newPass1 = trim(strip_tags($request['content']->newPass1));
    $newPass2 = trim(strip_tags($request['content']->newPass2));
    if($user = $service->getCurrentUser()){
        if($service->changePassword($oldPasswd, $newPass1, $newPass2)){
            print('{"result":"ok"}');
        }
        else{
            print('{"result":"error"}');
        }

    }
    else{
        http_response_code(401);
    }
    exit();
}


/*if($request['name'] == 'deleteUser'){
    Auth::authorize();
    exit();
}*/

http_response_code(404);



?>