<?php
require_once('../business/UserBusiness.php');
require_once('../ruler/Auth.php');
//session_start();
class UserRuler{

    public function registration($email, $name, $passwd){
        
        try{
            $user = UserBusiness::createUser($email, $name, $passwd);
            if(!$user){
                return false;
            }
            mail($user->getEmail(), 'Mozgóképszínház aktiváció', 'Kérjük kattintson ie az aktiváláshoz....'); //TODO: aktivációs link
            return true;
        }
        catch(Exception $ex){
            print_r($ex);
        }
        return false;
    }

    public function getUserByEmail(string $email){
        return new UserBusiness($email);
    }

    public function login(string $email, string $passwd){
        $user = new UserBusiness($email, $passwd);
        $_SESSION['userEmail'] = $user->getEmail();
        return $user;
    }

    public function getSession(){
        return $_SESSION;
    }

    public function getCurrentUser(){
       $user = new UserBusiness($_SESSION['userEmail']);
       if($user != null){
            return $user;
       }
       return false;
    }

    public function changePassword($oldPasswd, $newPass1, $newPass2){
        if($newPass1 == $newPass2){
            $userRuler = new UserRuler();
            $user = $userRuler->getCurrentUser();
            $user->setPasswd($oldPasswd, $newPass1);
            return true;
        }
        else{
            return false;
        }
    }

    //public function deleteUser(){} Házi feladat


}


?>