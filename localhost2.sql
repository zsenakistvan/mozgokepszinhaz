-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Gép: localhost:8889
-- Létrehozás ideje: 2022. Dec 08. 10:31
-- Kiszolgáló verziója: 5.7.34
-- PHP verzió: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `dualis`
--
CREATE DATABASE IF NOT EXISTS `dualis` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dualis`;

DELIMITER $$
--
-- Eljárások
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `createUser` (IN `email_in` VARCHAR(200) CHARSET utf8, IN `name_in` VARCHAR(200) CHARSET utf8, IN `created_on_in` VARCHAR(20) CHARSET utf8, IN `updated_on_in` VARCHAR(20) CHARSET utf8, IN `gts_in` VARCHAR(20) CHARSET utf8)  INSERT INTO `user` (`user`.`email`, `user`.`name`, `user`.`created_on`, `user`.`updated_on`, `user`.`gts`) VALUE (email_in, name_in, created_on_in, updated_on_in, gts_in)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByEmail` (IN `email_in` VARCHAR(200) CHARSET utf8)  SELECT * FROM `user` WHERE `user`.`email` = email_in$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beruhazas`
--

CREATE TABLE `beruhazas` (
  `id` int(11) NOT NULL,
  `nev` varchar(200) NOT NULL,
  `max_ertek` int(11) NOT NULL,
  `kivitelezes_datuma` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `beruhazas`
--

INSERT INTO `beruhazas` (`id`, `nev`, `max_ertek`, `kivitelezes_datuma`) VALUES
(1, 'Pénztáros Lölő autógumijának kiszúrása', 200000, '2022-12-06');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `palyazat`
--

CREATE TABLE `palyazat` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(200) NOT NULL,
  `beruhazas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `palyazat`
--

INSERT INTO `palyazat` (`id`, `azonosito`, `beruhazas_id`) VALUES
(2, 'd95f86f8-60f7-11ed-b62d-7e48ebc51a31', 1),
(4, 'b5118ba8-60fb-11ed-b62d-7e48ebc51a31', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_on` varchar(20) NOT NULL,
  `updated_on` varchar(20) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `level` int(1) NOT NULL DEFAULT '0',
  `gts` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `created_on`, `updated_on`, `is_active`, `level`, `gts`) VALUES
(1, 'sjdfbk', 'sjkahgf', '122142', '12123132', 0, 0, '124412214124'),
(2, 'alma@korte.hu', 'Kiss Béla Rendszergazda', '1670489878', '1670489878', 0, 0, '1670489878');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `beruhazas`
--
ALTER TABLE `beruhazas`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `palyazat`
--
ALTER TABLE `palyazat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `azonosito` (`azonosito`),
  ADD KEY `palyazat_beruhazas_fk` (`beruhazas_id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `beruhazas`
--
ALTER TABLE `beruhazas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `palyazat`
--
ALTER TABLE `palyazat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `palyazat`
--
ALTER TABLE `palyazat`
  ADD CONSTRAINT `palyazat_beruhazas_fk` FOREIGN KEY (`beruhazas_id`) REFERENCES `beruhazas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
