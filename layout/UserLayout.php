<?php
require_once('../business/UserBusiness.php');
require_once('LevelLayout.php');
class UserLayout{

    public $id;
    public $email;
    public $name;
    public $level;
    public $createdOn;
    public $updatedOn;
    public $gts;

    public function __construct(UserBusiness $userBusiness){
        $this->id = $userBusiness->getId();
        $this->email = $userBusiness->getEmail();
        $this->name = $userBusiness->getName();
        $this->level = LevelLayout::$levelTitle[$userBusiness->getLevel()];
        $this->createdOn = date('r', $userBusiness->getCreatedOn());
        $this->updatedOn = date('r', $userBusiness->getUpdatedOn());
        $this->gts = date('r', $userBusiness->getGts());
    }


}



?>