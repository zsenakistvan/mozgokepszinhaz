<?php
require_once('../entity/User.php');

class UserBusiness{

    private $id;
    private $email;
    private $name;
    private $level;
    private $isActive;
    private $createdOn;
    private $updatedOn;
    private $gts;

    public static function createUser($email, $name, $passwd){
        $success = User::createUser($email, $name, $passwd);
        if(!$success){
            return false;
        }
        $user = new User($email);
        $ub = new UserBusiness();
        $ub->email = $user->getEmail();
        $ub->name = $user->getName();
        $ub->level = $user->getLevel();
        $ub->isActive = $user->isActive();//-1, 0: false, amúgy true
        $ub->id = $user->getId();
        $ub->createdOn = $user->getCreatedOn();
        $ub->updatedOn = $user->getUpdatedOn();
        $ub->gts = $user->getGts();
        return $ub;
    }

    public function __construct(string $email = null, string $passwd = null){
        if ($passwd == null) {
            $user = new User($email);
        }
        else{
            $user = new User($email, $passwd);
        }
        $this->email = false;
        if ($user != null && $user != false && $user->getEmail() != false) {
            $this->email = $user->getEmail();
            $this->name = $user->getName();
            $this->level = $user->getLevel();
            $this->isActive = $user->isActive();
            $this->id = $user->getId();
            $this->createdOn = $user->getCreatedOn();
            $this->updatedOn = $user->getUpdatedOn();
            $this->gts = $user->getGts();
        }
    }

    public function setName($newName){
        $this->name = $newName;
        //$this->updatedOn = time();
        $user = new User($this->email);
        $user->setName($this->name);
    }

    public function acceptGts(){
        //$now = time();
        //$this->gts = $now;
        //$this->updatedOn = $now;
        $user = new User($this->email);
        $user->setGts();
    }

    public function deleteUser(){
        //$this->isActive = false;
        //$this->updatedOn = time();
        $user = new User($this->email);
        $user->delete();
    }

    public function activateUser(){
        $this->isActive = true;//TODO: deleted_at helyett -1, 0, 1 
        //$this->updatedOn = time();
        $user = new User($this->email);
        $user->activate();
    }

    public function setPasswd($oldPasswd, $newPasswd){
        $user = new User($this->email);
        $user->setPasswd($oldPasswd, $newPasswd);
    }

    //TODO: Level class, dt, setter


    public function getId(){
        return $this->id;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getName(){
        return $this->name;
    }

    public function getLevel(){
        return $this->level;
    }

    public function isActive(){
        return $this->isActive;
    }

    public function getCreatedOn(){
        return $this->createdOn;
    }

    public function getUpdatedOn(){
        return $this->updatedOn;
    }

    public function getGts(){
        return $this->gts;
    }

}



?>