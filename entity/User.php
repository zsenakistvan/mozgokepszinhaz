<?php
require_once('framework/StoredProcedureQuery.php');
class User{
    /**
     * auto_increment
     */
    private $id;
    /**
     * unique
     */
    private $email;
    private $name;
    private $createdOn;
    private $updatedOn;
    private $isActive;
    private $level;
    private $gts;
    private $passwd;

    public static function createUser($email, $name, $passwd){
        $createUser = new StoredProcedureQuery('createUser');
        $createUser->setParameter('email_in', $email);
        $createUser->setParameter('name_in', $name);
        $now = time();
        $createUser->setParameter('created_on_in', $now);
        $createUser->setParameter('updated_on_in', $now);
        $createUser->setParameter('gts_in', $now);
        $createUser->setParameter('passwd_in', $passwd);
        $result = $createUser->execute();
        return $result;
    }

    public function __construct($email, $passwd = null){
        if ($passwd == null) {
            $getUserByEmail = new StoredProcedureQuery('getUserByEmail');
            $getUserByEmail->setParameter('email_in', $email);
            $user = $getUserByEmail->execute();
        }
        else{
            $login = new StoredProcedureQuery('login');
            $login->setParameter('email_in', $email);
            $login->setParameter('passwd_in', $passwd);
            $user = $login->execute();
        }
        $this->email = false;
        if ($user != false) {
            $this->email = $user['email'];
            $this->id = $user['id'];
            $this->name = $user['name'];
            $this->level = $user['level'];
            $this->isActive = $user['is_active'];
            $this->updatedOn = $user['updated_on'];
            $this->createdOn = $user['created_on'];
            $this->gts = $user['gts'];
            $this->passwd = $user['passwd'];
        }
    }

    public function getId(){
        return $this->id;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getName(){
        return $this->name;
    }

    public function getLevel(){
        return $this->level;
    }

    public function isActive(){
        return $this->isActive;
    }

    public function getCreatedOn(){
        return $this->createdOn;
    }

    public function getUpdatedOn(){
        return $this->updatedOn;
    }

    public function getGts(){
        return $this->gts;
    }

    private function updateUser(){
        $updateUser = new StoredProcedureQuery('updateUser');
        $updateUser->setParameter('id_in', $this->id);
        $updateUser->setParameter('name_in', $this->name);
        $updateUser->setParameter('email_in', $this->email);
        $updateUser->setParameter('is_active_in', $this->isActive);
        $updateUser->setParameter('level_in', $this->level);
        $updateUser->setParameter('gts_in', $this->gts);
        $updateUser->setParameter('passwd_in', $this->passwd);
        $updateUser->setParameter('updated_on_in', time());
        $updateUser->execute();
    }

    public function setName($newName){
        $this->name = $newName;
        $this->updateUser();
    }

    public function setEmail($newEmail){
        $this->email = $newEmail;
        $this->updateUser();
    }

    public function activate(){
        $this->isActive = 1;
        $this->updateUser();
    }

    /*public function delete(){
        $this->isActive = 0;
        $this->updateUser();
    }*/

    public function setLevel($newLevel){
        $this->level = $newLevel;
        $this->updateUser();
    }

    public function setGts(){
        $this->gts = time();
        $this->updateUser();
    }

    public function setPasswd($oldPasswd, $newPasswd){
        if($this->passwd == sha1($oldPasswd)){
            $this->passwd = $newPasswd;
            $this->updateUser();
            return true;
        }
        else{
            return false;
        }
    }

    public function delete(){
        $deleteUser = new StoredProcedureQuery('deleteUser');
        $deleteUser->setParameter('id_in', $this->id);
        $deleteUser->execute();
    }
    
}


?>