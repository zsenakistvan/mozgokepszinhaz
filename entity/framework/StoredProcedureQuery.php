<?php
class StoredProcedureQuery{
//PDO init:
private $host = 'localhost';
private $port = '8875';//def: 3306
private $db = 'dualis';
private $user = 'dualis';
private $password = 'dualis';
private $pdo;

//SPQ params:
private $spqName;
private $params;

public function __construct($spqName){
    $params = array();
    $this->spqName = $spqName;
    try {
        $this->pdo = new PDO('mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->db, $this->user, $this->password ,array(
            PDO::ATTR_DEFAULT_FETCH_MODE =>PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', 
        ));
        //print 'Sikeres csatlakozás';
    } catch (PDOException $ex) {
        print "Error!: " . $ex->getMessage() . "<br/>";
        die();
    }
}

public function setParameter($paramName, $paramValue){
$this->params[] = array('paramName' => $paramName, 'paramValue' => $paramValue);
}

public function execute(){
    $sql = 'CALL ' . $this->spqName . '(';
    for($i = 0; $i < count($this->params); $i++) {
        if($i == count($this->params) -1){
            $sql .= ':' . $this->params[$i]['paramName'] .')';
        }
        else{
            $sql .= ':' . $this->params[$i]['paramName'] .', ';
        }
    }
    //print $sql;
    $spq = $this->pdo->prepare($sql);
    //print_r($spq->errorInfo());
    $command = array();
    for ($i=0; $i < count($this->params); $i++) { 
        $command[':' . $this->params[$i]['paramName']] = $this->params[$i]['paramValue'];
    }
    //print_r($command);
    try {
            //print_r($command);
        $spq->execute($command);
       // print_r($spq->errorInfo());
    } catch (Exception $ex) {
        print "Error!: " . $ex->getMessage() . "<br/>";
        die();
    }
    return $spq->fetch(PDO::FETCH_ASSOC);
}

}
/*

$sth->execute([
    ':quantity' => 12,
    ':name' => 'ethan',
    ':barcode' => '987261826671',
]);


*/ 

?>